// Sergio Segrera 1833693
package conflict;

public class BikeStore {

	public static void main(String[] args) {
		Bicycle[] bicycles = new Bicycle[4];
		
		bicycles[0] = new Bicycle("Trek", 24, 50);
		bicycles[1] = new Bicycle("Giant", 0, 60);
		bicycles[2] = new Bicycle("Shimano", 14, 30);
		bicycles[3] = new Bicycle("Santa Cruz", 24, 70);
		
		for (int i = 0; i < bicycles.length; i++) {
			System.out.println(bicycles[i]);
		}
	}

}
